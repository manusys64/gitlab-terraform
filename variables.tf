# variable "ami" {
#   type        = string
#   description = "Ubuntu AMI ID in eu-central-1 Region"
#   default     = "ami-065deacbcaac64cf2"
# }

# variable "instance_type" {
#   type        = string
#   description = "Instance type"
#   default     = "t2.micro"
# }

# variable "name_tag" {
#   type        = string
#   description = "Name of the EC2 instance"
#   default     = "My EC2 Instance"
# }

variable "awsprops" {
    type = map(string)
    default = {
      region = "us-central-1"
      vpc = "vpc-078d9789f65862ce0"
      ami = "ami-065deacbcaac64cf2"
      itype = "t2.micro"
      subnet = " subnet-03d1e8ae61e54b14b"
      publicip = true
  }
}