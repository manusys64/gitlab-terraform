output "instance_id" {
  value       = aws_instance.project-iac.id
  description = "Instance ID"
}

output "ec2instance" {
  value = aws_instance.project-iac.public_ip
}
